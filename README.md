# Test Servier

Technical test for DS position at Servier, realized by Bruno Siarry with LittleBigCode.

## Description
In this test, we will establish a model to predict the 
targets P1 and [P1-P9] thanks to a molecule's structure
described by its *smiles* string. We will first pretreat
the data, train and evaluate a model. This model will be
integrated withing a packaged program, with an API, 
access points, and a docker image.

### Data and models
The data to study for the test is in two CSV files dataset_single and multi, each of 4999 lines.
For each molecule we have a unique *id*, the *P1* property to predict (or [*P1-P9*] in the multi file),
as well as a *smile* string, describing the molecule.
This *smile* string is going to be the basis for the feature vector for our 3 prediction models : 

1. In the model 1 : we use the feature_extractor.py written with the package rdkit, made to translate the smile string
into a (2048,1) feature vector. This vector is fed into a Convolutionnal Neural Network (summary bellow).
The use of a CNN is justified by the high dimensionality of the input vector, and its binary nature. 
We want to extract the main properties of the vector thanks to 10 channels of 1D Convolutions, and reduce its 
dimensionality (thanks to MaxPooling layers), before feeding it to a Multi Layer Perceptron ([200, 100, 1]).
2. In the model 2 we have to create are own embedding of the smiles string. Given the limited time
and limited access to domain expertise, I choose to do a very simple embedding : counting each type of character in the smiles string.
This gives a (29,1) feature vector (from the 29 symbols appearing in this dataset). This feature vector goes through a 
simple Multi Layer Perceptron : with [128, 256, 256, 128, 1] nodes.
3. The model 3 is exactly analog to model 1 (as the input feature vector is the same) except for its last layer (or *head*).
The head is a Dense layer with 9 nodes (to predict *[P1-P9]*), with a softmax activation (instead of Logistic in model1).

The specifics of the data, and the results of the model will not be discussed here (but can be accessed through the 
`Exploration and modelling` notebook). 

### Application and Packaging
The models are embedded into an application which installation are described bellow.
The application has a *setup.py* file, a *Dockerfile* and a *Flask API*.

## Repository structure
The structure of the repository is at follows : 
```
│   .dockerignore
│   .gitignore
│   conda_env.yml
│   dockerfile
│   main.py
│   README.md
│   requirements.txt
│   setup.py
│   __init__.py
│
├───data
│       dataset_multi.csv
│       dataset_single.csv
│
├───notebooks
│   │   Exploration and modeling.ipynb
│   │
│   └───.ipynb_checkpoints
│           Exploration and modeling-checkpoint.ipynb
├───src
│   │   config.py
│   │   __init__.py
│   │
│   ├───application
│   │   │   api_server.py
│   │   │   evaluate.py
│   │   │   predict.py
│   │   │   train.py
│   │   └───__init__.py
│   │  
│   ├───domain
│   │   │   models.py
│   │   │   __init__.py
│   │   └───saved_models
│   │
│   └───infrastructure
│       │   data_preparation.py
│       │   feature_extractor.py
│       └───__init__.py
└───────test
```

The documentations and installation file are at the root of the folder, then the python code is mainly in the `src` folder.
While the data (not on repo) has to be placed into a folder at the root directory. 

## Install and use

### Install packages
The necessary environment and libraries can be installed thanks to the following:

```
# Create a conda env thanks to the command : 
conda create -n servierenv python=3.6 

# Activate the conda env
conda activate servierenv

# Installing main packages 
pip install -r requirements.txt

# Installing the rdkit package through conda-forge
conda install -c conda-forge rdkit

# Installing the local package and entrypoints
python ./setup.py install
```

*Warning* : some users have seen issues with the pandas installation through pip.
You can reinstall pandas through : `conda install pandas`

### Locally run the application
The main elements to run the application locally are :

* The config file in `./src/config.py`. Sets all values and default parameters for the program.
* The `setup.py` installs 4 entrypoints. The available commands are : 
  * `train`: to train a model on the inputted data.
  * `evaluate`: to evaluate the model on the inputted data
  * `predict`: to obtain predictions
  * `api_run`: runs the flask api
* There are 3 parameters available (for the first 3 commands):
  * `data` with the path to the csv file to use for the specific operation, 
  * `modelpath` with the path to the keras model saved (or to be saved for train)
  * `modeltype` : integer in 1 to 3 with the model type (see numbers above)

*Exemple commands :* 

`train --data ./data/dataset_single.csv --modelpath ./src/domain/model --modeltype 1
`
This command will train a model of type 1 on the mydata.csv, and export it to model.pt file.

`predict --data ./data/dataset_single.csv --modelpath ./src/domain/model --modeltype 1`
This command will load a model of type 2 and generate predictions for the whole mydata.csv file.

### Docker
* Dockerfile content : the code and the installation, but no data.

* BUILD with : `docker build . -t serviertestbruno`

* RUN with : `docker run --mount type=bind,source="$(pwd)"/data,target=/servier_entry/data -p 5000:5000 -it serviertestbruno`

### Flask API:
* Exposed on port 5000 (set in config), and with the model specified in config
* Started by command : `api_run` with no parameters.
* Endpoints available: "/" and "/predict":
  * `"/"` is the welcome endpoint, to show a message and confirm running
  * `"/predict"` is the endpoint for prediction to be used in GET method to obtain prediction
  
* The GET request format to obtain the predictions from the /predict route is: 
  
*Exemple request :* 
`curl --location --request GET '192.168.1.96:5000/predict' \
--header 'Content-Type: application/json' \
--data-raw '{
  "smiles": ["Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C", "Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C"]
}'`


## Organisation and next steps

1. Write tests
2. Do proper exports (not so useful)
3. Set precommit config (a bit late)

## Ownership
**Code :** Bruno Siarry @ LittleBigCode

**Data and test :** Servier

