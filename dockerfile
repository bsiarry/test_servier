FROM continuumio/miniconda3:4.11.0

COPY . /servier_entry
WORKDIR /servier_entry

# Environment
RUN apt-get update
EXPOSE 5000

# create and activate conda env
RUN conda init bash && \
    . /root/.bashrc && \
    conda update -n base conda && \
    conda create -y --name servier_env python=3.6 && \
    conda activate servier_env

# pip upgrade
RUN conda install pip &&\
    pip install --upgrade pip

# requirements
RUN pip install -r ./requirements.txt
RUN conda install -c conda-forge rdkit

# Local package setup
RUN ["python", "./setup.py", "install"]

# Test
#RUN ["train"]
#ENTRYPOINT ["python", "-c", "from src.config import DATA_PATH_SINGLE"]

