import src.config as config
from src.application.train import train_model
from src.application.evaluate import evaluate_perf
from src.infrastructure.data_preparation import SmileData


if __name__ == "__main__":
    print("----- Beginning of main script -----")

    model_export_path, data_path, model_type = config.generic_parser("main")
    print(
        f"Training on data {data_path}, with model exported to {model_export_path} of type {model_type}"
    )

    data_smiles = SmileData(path_to_csv=data_path, model_number=model_type)

    data_smiles.convert_smiles_features(smiles_col_name="smiles")
    data_smiles.split_data(valid_ratio=0.2, test_ratio=0.1)

    my_model = train_model(
        smiles_features=data_smiles.train_data[0],
        target=data_smiles.train_data[1],
        validation_tuple=data_smiles.valid_data,
        model_nb=model_type,
        epochs=config.NB_EPOCHS,
        save_path=model_export_path,
    )

    evaluate_perf(my_model, data_smiles.test_data)

    print("----- End of main script -----")
