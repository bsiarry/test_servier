from setuptools import setup, find_packages

setup(
    name="testServier",
    version="1.0",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "train = src.application.train:train",
            "evaluate = src.application.evaluate:evaluate",
            "predict = src.application.predict:predict",
            "api_run = src.application.api_server:run_api",
        ],
    },
)
