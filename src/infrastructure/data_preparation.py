import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from .feature_extractor import fingerprint_features
from src.config import TARGET_NAMES_SINGLE, TARGET_NAMES_MULTI


def encode_api_request(json_get_input, smiles_col_name: str = "smiles"):
    """
    Specific to treat the incoming requests and prepare them for the model.
    :param json_get_input: body of API GET request
    :param smiles_col_name: name of the json key containing smiles string
    :return: features list, ready for prediction by the model
    """
    df_in = pd.DataFrame.from_dict(json_get_input)
    print(df_in)
    features_input = np.array(
        df_in[smiles_col_name]
        .apply(fingerprint_features)
        .apply(lambda x: [[int(char)] for char in x.ToBitString()])
        .to_list()
    )
    return features_input


class SmileData:
    """
    Class to open, pretreat and prepare the molecules smiles data for models
    Target can be specified at instanciation (single is one element list, or multiple).
    This class is fairly simple, but could be more complex (in case of NaNs, or more pretreatment).
    Mainly contains methods to split train/val/test data with the class (eventually to keep track of split),
    and a method to convert the smiles strings into features.
    """

    def __init__(self, path_to_csv: str, model_number: int = 1):
        self.model_number = model_number
        self.target_names = (
            TARGET_NAMES_MULTI if model_number == 3 else TARGET_NAMES_SINGLE
        )
        self.data_raw = pd.read_csv(path_to_csv)
        self.target = self.data_raw[self.target_names]
        self.features_array = None
        self.train_data = None
        self.valid_data = None
        self.test_data = None

    def convert_smiles_features(self, smiles_col_name: str = "smiles"):
        """
        Function to apply pretreatments and prepare features (taken out of init to gain generality)

        :param model_number: Gives model number to adapt the feature preparation
        :param smiles_col_name: In case we get a files with a distinct column name
        """
        if self.model_number == 1 or self.model_number == 3:
            self.features_array = np.array(
                self.data_raw[smiles_col_name]
                .apply(fingerprint_features)
                .apply(lambda x: [[int(char)] for char in x.ToBitString()])
                .to_list()
            )

        elif self.model_number == 2:
            SYMBOL_SMILES = {
                "2",
                "H",
                "c",
                "6",
                "+",
                "N",
                "s",
                "(",
                "C",
                "n",
                "\\",
                "-",
                "#",
                "B",
                "3",
                "F",
                "5",
                "S",
                "=",
                "4",
                "1",
                "[",
                "/",
                "o",
                "O",
                ")",
                "l",
                "r",
                "]",
            }
            self.features_array = np.array(
                [
                    [smiles.count(s) for s in SYMBOL_SMILES]
                    for smiles in self.data_raw[smiles_col_name].values
                ]
            )

    def split_data(self, valid_ratio: float = 0.2, test_ratio: float = 0.1):
        """
        This function may be overkill here, but allows to keep track of the split and dataset used within the class
        :param valid_ratio: float, ratio of total dataset for validation set
        :param test_ratio: float, ratio of total dataset for test set
        """

        assert self.features_array is not None, "Data was not pretreated"

        train_ft, temp_ft, train_target, temp_target = train_test_split(
            self.features_array, self.target, test_size=(test_ratio + valid_ratio)
        )

        valid_ft, test_ft, valid_target, test_target = train_test_split(
            temp_ft, temp_target, test_size=test_ratio / (test_ratio + valid_ratio)
        )

        self.train_data = (train_ft, train_target)
        self.valid_data = (valid_ft, valid_target)
        self.test_data = (test_ft, test_target)
