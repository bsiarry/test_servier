import argparse

## MODEL PATH
MODEL_PATH = "./src/domain/saved_models"
MODEL_TYPE_NB = 3

## DATA COLs
FEATURE_NAME = "smiles"
TARGET_NAMES_SINGLE = ["P1"]
TARGET_NAMES_MULTI = [f"P{i}" for i in range(1, 10)]

## TRAINIGN PARAMS
DATA_PATH_SINGLE = "./data/dataset_single.csv"
DATA_PATH_MULTI = "./data/dataset_multi.csv"
INPUT_SHAPE = (2048, 1)
INPUT_SHAPE_MODEL_2 = (None, 29)

# LEARNING PARAMS
BATCH_SIZE = 100
NB_EPOCHS = 10
ADAM_RATE = 0.03

## CNN Config
# Conv1D Layers
CONV_KERNEL_SIZE = 9
N_CONV_FILTER = 10
CONV_REGULARIZATION = "l2"
STRIDES = 1
PADDING = "valid"
# Pooling Layers
POOL_SIZE = int(CONV_KERNEL_SIZE / 4)
# DENSE LAYERS
MLP_1_NODES = 200
MLP_2_NODES = 100
MLP_DROPOUT = 0.5

## API CONFIG
API_PORT = "5000"
API_HOST = "0.0.0.0"


def generic_parser(context: str):
    """
    Simple parser that can be used for all must have functionality
    Defining functions in config file may not be the best, but makes sense in terms of DDD
    :param: context allows to easily change help message
    :return: path_to_model, path_to_data, model_nb : 3 main params for the problem
    """

    parser = argparse.ArgumentParser(
        description=f"Indicate model and data for {context}."
    )

    parser.add_argument(
        "--modelpath",
        type=str,
        default=MODEL_PATH,
        help=f"Path to model to use for {context}",
    )
    parser.add_argument(
        "--data", type=str, default=None, help=f"Path to data to use for {context}"
    )
    parser.add_argument(
        "--modeltype", type=int, default=MODEL_TYPE_NB, help="Model type (1, 2 or 3)"
    )

    args = parser.parse_args()
    path_to_model, path_to_data, model_nb = args.modelpath, args.data, args.modeltype
    if not path_to_data:
        path_to_data = DATA_PATH_MULTI if model_nb == 3 else DATA_PATH_SINGLE

    return path_to_model, path_to_data, model_nb
