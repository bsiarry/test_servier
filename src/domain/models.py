import keras
import tensorflow as tf

from keras.models import Sequential
from keras.layers import (
    Dense,
    Dropout,
    Flatten,
    Lambda,
    Conv1D,
    MaxPooling1D,
    MaxPooling2D,
)

import src.config as config


def model_cnn_single():
    """
    Definition of the CNN model of type 1
    Set from config :
        - the optimizer : binary_crossentropy
        - the activation functions : relu, except sigmoid for last neuron
        - the other parameters can be fixed from the config file
    return: keras.Model
    """
    # Create Model
    input_shape = config.INPUT_SHAPE
    model = Sequential()
    print("INPUT====>", input_shape)
    # Conv1D Layer
    model.add(
        Conv1D(
            filters=config.N_CONV_FILTER,
            kernel_size=config.CONV_KERNEL_SIZE,
            activation="relu",
            padding=config.PADDING,
            strides=config.STRIDES,
            kernel_regularizer=config.CONV_REGULARIZATION,
            input_shape=input_shape,
        )
    )
    # Maxpool layer
    model.add(MaxPooling1D(pool_size=config.POOL_SIZE, padding=config.PADDING))

    # Conv1D Layer
    model.add(
        Conv1D(
            filters=config.N_CONV_FILTER,
            kernel_size=config.CONV_KERNEL_SIZE,
            activation="relu",
            padding=config.PADDING,
            strides=config.STRIDES,
            kernel_regularizer=config.CONV_REGULARIZATION,
        )
    )
    # Maxpool layer
    model.add(MaxPooling1D(pool_size=config.POOL_SIZE, padding=config.PADDING))
    # Flatten
    model.add(Flatten())
    # MLP (2 layers)
    model.add(Dense(config.MLP_1_NODES, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))
    model.add(Dense(config.MLP_2_NODES, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    # Classification layer
    model.add(Dense(1, activation="sigmoid"))

    print(model.summary())

    # Compiler le modèle
    model.compile(
        loss=keras.losses.binary_crossentropy,
        optimizer=tf.keras.optimizers.Adam(learning_rate=config.ADAM_RATE),
        metrics=["accuracy", tf.keras.metrics.FalsePositives()],
    )

    return model


def model_cnn_multi():
    """
    Definition of the CNN model of type 3 (FOR THE MULTI CASE)
    Set from config :
        - the optimizer : binary_crossentropy
        - the activation functions : relu, except sigmoid for last neuron
        - the other parameters can be fixed from the config file
    return: keras.Model
    """
    # Create Model
    input_shape = config.INPUT_SHAPE
    model = Sequential()

    # Conv1D Layer
    model.add(
        Conv1D(
            filters=config.N_CONV_FILTER,
            kernel_size=config.CONV_KERNEL_SIZE,
            activation="relu",
            padding=config.PADDING,
            strides=config.STRIDES,
            kernel_regularizer=config.CONV_REGULARIZATION,
            input_shape=input_shape,
        )
    )

    # Maxpool layer
    model.add(MaxPooling1D(pool_size=config.POOL_SIZE, padding=config.PADDING))

    # Conv1D Layer
    model.add(
        Conv1D(
            filters=config.N_CONV_FILTER,
            kernel_size=config.CONV_KERNEL_SIZE,
            activation="relu",
            padding=config.PADDING,
            strides=config.STRIDES,
            kernel_regularizer=config.CONV_REGULARIZATION,
            input_shape=input_shape,
        )
    )

    # Maxpool layer
    model.add(MaxPooling1D(pool_size=config.POOL_SIZE, padding=config.PADDING))

    # Flatten
    model.add(Flatten())

    # MLP (2 layers)
    model.add(Dense(config.MLP_1_NODES, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    model.add(Dense(config.MLP_2_NODES, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    # Classification layer
    model.add(Dense(9, activation="softmax"))

    print(model.summary())

    # Compiler le modèle
    model.compile(
        loss=keras.losses.binary_crossentropy,
        optimizer=tf.keras.optimizers.Adam(learning_rate=config.ADAM_RATE),
        metrics=["accuracy"],
    )

    return model


def model_str_single():
    """
    Model 2 in the problem definition.
    Approach: we convert the string into a feature vector of the count of each character possible (within the dataset).
    This feature vector is fed into this MLP to predict P1.
    This model does not "learn" much, as the loss barely decreases ... This features preparation may not be the best idea.
    :return: keras.Model
    """
    # Create Model
    model = Sequential()
    input_shape = config.INPUT_SHAPE_MODEL_2

    # Flatten
    # model.add(Flatten())

    # MLP (4 layers)
    model.add(Dense(128, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    model.add(Dense(256, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    model.add(Dense(256, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    model.add(Dense(128, activation="relu"))
    model.add(Dropout(config.MLP_DROPOUT))

    # Classification layer
    model.add(Dense(1, activation="sigmoid"))

    model.build(input_shape)
    print(model.summary())

    # Compile model
    model.compile(
        loss=keras.losses.binary_crossentropy,
        optimizer=tf.keras.optimizers.Adam(learning_rate=config.ADAM_RATE),
        metrics=["accuracy", tf.keras.metrics.FalsePositives()],
    )

    return model
