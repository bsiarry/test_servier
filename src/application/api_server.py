from time import time, asctime
import datetime

from tensorflow import keras
from flask import Flask, jsonify, request

import src.config as config
from src.infrastructure.data_preparation import encode_api_request

app = Flask(__name__)
port, host = config.API_PORT, config.API_HOST

model_loaded = keras.models.load_model(config.MODEL_PATH)


@app.route("/", methods=["GET"])
def welcome_message():
    """
    Returns a message to confirm connexion to the API.
    """
    answer = (
        "Welcome to Bruno's test. Please refer to documentation for route descriptions"
    )
    documentation = "https://gitlab.com/bsiarry/testServier"
    response = {"answer": answer, "documentation": documentation}
    return jsonify(response)


@app.route("/predict", methods=["GET"])
def predict():
    """
    Main API route: executes Flask GET with the predict pipeline
    Obtains the Json from the request, and predicts it with classifier
    Possible to send a
    Request body : json with at least the "smiles" key with for value
    a list of smiles strings.
    # TODO : add validation method for body format, REFACTOR, WORKS FOR MULTI ENTRY ONLY SO FAR
    Returns:
    --------
        json: {answer: list with the result(s) of the prediction,
                info: message relative to execution,
                json_info: message relative to the json}
    """
    start = time()
    json_message = "JSON payload not validated"
    json_okay = True  # TODO : json checker

    json_data = request.get_json()
    if json_okay:
        json_message = "JSON payload OK"
        input_val = encode_api_request(json_data)

        if len(input_val) == 1:
            val_for_predict = input_val.reshape(1, -1)
            answer = model_loaded.predict(val_for_predict)[0].tolist()
            exec_message = f"Prediction generated at {asctime()}"

        elif len(input_val) > 1:
            val_for_predict = input_val
            answer = model_loaded.predict(val_for_predict).tolist()
            exec_message = f"Prediction generated at {asctime()}"

    response = {"answer": answer, "info": exec_message, "json_info": json_message}

    end = time()
    print("Result request : ", response)
    print(f"Duration of request : {(end - start)}")
    return jsonify(response)


def run_api():
    """
    Start the API
    """
    print("Starting API at", datetime.datetime.now())
    print(f"Running the API with the model in {config.MODEL_PATH}")
    app.run(debug=False, host=host, port=port)


if __name__ == "__main__":
    run_api()
