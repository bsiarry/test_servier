import argparse
from sklearn.metrics import roc_auc_score, auc, classification_report, confusion_matrix
import keras

import src.config as config
from src.infrastructure.data_preparation import SmileData


def evaluate():
    """
    Encapsulates the evaluation pipeline, with args from parser and config
    :return:
    """

    print("----- Beginning of evaluation script -----")

    model_path, eval_data_path, model_type = config.generic_parser("evaluation")
    print(
        f"Evaluating on data {eval_data_path}, with model at {model_path} of type {model_type}"
    )

    data_smiles = SmileData(path_to_csv=eval_data_path, model_number=model_type)

    data_smiles.convert_smiles_features(smiles_col_name=config.FEATURE_NAME)

    model = keras.models.load_model(model_path)
    evaluate_perf(model, (data_smiles.features_array, data_smiles.target))

    print("----- End of evaluation script -----")


def evaluate_perf(model_eval, test_data):
    """
    Computes AUC score
    """
    test_target = test_data[1]
    perf_test_proba = model_eval.predict(test_data[0])
    auc_score = roc_auc_score(test_target, perf_test_proba)
    print(f"The AUC of the model is : {auc_score}")  # TODO : export in file or class
    return auc_score


def export_perf_report(path_export):
    """
    TODO
    Exports the metrics defined
    :param path_export:
    """


if __name__ == "__main__":
    evaluate()
