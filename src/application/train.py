import src.config as config
from src.infrastructure.data_preparation import SmileData
from src.domain.models import model_cnn_single, model_str_single, model_cnn_multi


def train():
    """
    Main training function, to encapsulate the function bellow, recuperates args from parser and config file
    :return: nothing so far
    """
    print("----- Beginning of training script -----")

    model_export_path, data_path, model_type = config.generic_parser("training")
    print(
        f"Training on data {data_path}, with model exported to {model_export_path} of type {model_type}"
    )

    data_smiles = SmileData(path_to_csv=data_path, model_number=model_type)

    data_smiles.convert_smiles_features(smiles_col_name=config.FEATURE_NAME)
    data_smiles.split_data(valid_ratio=0.2, test_ratio=0.1)

    my_model = train_model(
        smiles_features=data_smiles.train_data[0],
        target=data_smiles.train_data[1],
        validation_tuple=data_smiles.valid_data,
        model_nb=model_type,
        epochs=config.NB_EPOCHS,
        save_path=model_export_path,
    )

    print("----- End of training script -----")


def train_model(
    smiles_features,
    target,
    validation_tuple,
    model_nb: int = 1,
    epochs: int = 3,
    save_path: str = None,
):
    """
    Function to train a new model, from with input parameters

    :param smiles_features: array of features used for training
    :param target: array of target used for training
    :param validation_tuple: (features, target) for validation during training
    :param model_nb: type of model according to problem definition
    :param epochs: nb of epochs for training
    :param save_path: path to save the model weights
    :return: the trained model
    """

    model_fn = [model_cnn_single, model_str_single, model_cnn_multi][model_nb - 1]

    print(f"Creating and training a model with {model_fn}")
    model = model_fn()

    model.fit(
        x=smiles_features,
        y=target,
        validation_data=validation_tuple,
        epochs=epochs,
        verbose=1,
    )
    if save_path:
        model.save(save_path)
    return model


if __name__ == "__main__":
    train()
