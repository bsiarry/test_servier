import argparse
import keras

import src.config as config
from src.infrastructure.data_preparation import SmileData


def parser_eval():
    parser = argparse.ArgumentParser(
        description="Indicate model and data for prediction."
    )

    parser.add_argument(
        "--model",
        type=str,
        default=config.MODEL_PATH,
        help="Path to model to use for prediction",
    )
    parser.add_argument(
        "--data",
        type=str,
        default=config.DATA_PATH_SINGLE,
        help="Path to data to use for prediction",
    )
    parser.add_argument(
        "--modeltype",
        type=int,
        default=config.MODEL_TYPE_NB,
        help="Model type (1, 2 or 3)",
    )
    args = parser.parse_args()
    path_to_model, path_to_data, model_nb = args.export, args.data, args.modeltype
    return path_to_model, path_to_data, model_nb


def export_preds(path_export):
    """
    TODO
    Exports the metrics defined
    :param path_export:
    """


def predict():
    """
    Encapsulates the predict pipeline, with args from parser and config
    :return:
    """
    print("----- Beginning of prediction script -----")

    model_path, predict_data_path, model_type = config.generic_parser("prediction")
    print(
        f"Predicting data {predict_data_path}, with model {model_path} of type {model_type}"
    )

    data_smiles = SmileData(path_to_csv=predict_data_path, model_number=model_type)

    data_smiles.convert_smiles_features(smiles_col_name=config.FEATURE_NAME)

    model = keras.models.load_model(model_path)
    predictions = model.predict(data_smiles.features_array)

    print("The predictions are :", predictions)  # TODO : export

    print("----- End of prediction script -----")


if __name__ == "__main__":
    predict()
